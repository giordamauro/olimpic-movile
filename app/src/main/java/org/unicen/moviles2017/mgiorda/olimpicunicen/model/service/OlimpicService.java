package org.unicen.moviles2017.mgiorda.olimpicunicen.model.service;

import android.util.Log;

import org.unicen.moviles2017.mgiorda.olimpicunicen.network.OlimpicApiClient;
import org.unicen.moviles2017.mgiorda.olimpicunicen.network.dto.ActividadCulturalDto;
import org.unicen.moviles2017.mgiorda.olimpicunicen.network.dto.FacultadDto;
import org.unicen.moviles2017.mgiorda.olimpicunicen.network.dto.PartidoDto;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.ActividadCultural;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.DiaEventos;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.Evento;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.Partido;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class OlimpicService {

    private final SimpleDateFormat onlyDateFormat = new SimpleDateFormat("yyyyMMdd", new Locale("es", "AR"));
    private final OlimpicApiClient apiClient;

    public OlimpicService(OlimpicApiClient apiClient) {

        Objects.requireNonNull(apiClient, "apiClient can't be null");
        this.apiClient = apiClient;
    }

    public void getPartidos(final ServiceCallback<List<DiaEventos<Partido>>> callback) {

        Objects.requireNonNull(callback, "callback can't be null");

        apiClient.getPartidos(new ServiceCallback<List<PartidoDto>>() {

            @Override
            public void onSuccess(List<PartidoDto> dtoPartidos) {

                List<Partido> partidos = mapPartidos(dtoPartidos);
                List<DiaEventos<Partido>> partidosPorDia = getEventosPorDia(partidos);

                callback.onSuccess(partidosPorDia);
            }

            @Override
            public void onError(String message, Throwable exception) {
                callback.onError(message, exception);
            }
        });
    }

    public void getCulturales(final ServiceCallback<List<DiaEventos<ActividadCultural>>> callback) {

        Objects.requireNonNull(callback, "callback can't be null");

        apiClient.getCulturales(new ServiceCallback<List<ActividadCulturalDto>>() {

            @Override
            public void onSuccess(List<ActividadCulturalDto> dtoCulturales) {

                List<ActividadCultural> culturales = mapCulturales(dtoCulturales);
                List<DiaEventos<ActividadCultural>> culturalesPorDia = getEventosPorDia(culturales);

                callback.onSuccess(culturalesPorDia);
            }

            @Override
            public void onError(String message, Throwable exception) {
                callback.onError(message, exception);
            }
        });
    }

    public <E extends Evento> List<DiaEventos<E>> getEventosPorDia(List<E> eventos) {

        Objects.requireNonNull(eventos, "eventos can't be null");

        if(eventos.isEmpty()) {
            return Collections.emptyList();
        }

        Map<String, List<E>> dateMap = new HashMap<>();

        for (E evento : eventos) {

            String onlyDate = onlyDateFormat.format(evento.getFecha());
            List<E> dayPartidos = dateMap.get(onlyDate);
            if(dayPartidos == null) {
                dayPartidos = new ArrayList<>();
                dateMap.put(onlyDate, dayPartidos);
            }

            dayPartidos.add(evento);
        }

        List<DiaEventos<E>> eventosPorDia = new ArrayList<>();
        for (Map.Entry<String, List<E>> entry : dateMap.entrySet()) {

            try {
                Date fecha = onlyDateFormat.parse(entry.getKey());

                List<E> dayPartidos = entry.getValue();
                Collections.sort(dayPartidos);

                eventosPorDia.add(new DiaEventos<>(fecha, dayPartidos));

            } catch (ParseException e) {
                Log.e("olimpicservice", "Exception parsing date", e);
            }
        }

        Collections.sort(eventosPorDia);

        return eventosPorDia;
    }

    public List<Partido> mapPartidos(List<PartidoDto> dtoPartidos) {

        Objects.requireNonNull(dtoPartidos, "dtoPartidos can't be null");

        List<Partido> partidos = new ArrayList<>();

        for (PartidoDto partidoDto : dtoPartidos) {
            Partido partido = mapPartido(partidoDto);
            partidos.add(partido);
        }

        return partidos;
    }

    public Partido mapPartido(PartidoDto dto) {

        Objects.requireNonNull(dto, "dto can't be null");

        Partido partido = new Partido();
        partido.setDeporte(dto.getDeporte());
        partido.setFacultad1(dto.getFacultad1().getNombre());
        partido.setLugar(dto.getLugar());
        partido.setResultado(dto.getResultado());
        partido.setFecha(new Date(dto.getFecha()));

        FacultadDto facultadDto = dto.getFacultad2();
        partido.setFacultad2(facultadDto == null ? "<empty>" : facultadDto.getNombre());

        return partido;
    }

    public List<ActividadCultural> mapCulturales(List<ActividadCulturalDto> dtoCulturales) {

        Objects.requireNonNull(dtoCulturales, "dtoCulturales can't be null");

        List<ActividadCultural> culturales = new ArrayList<>();

        for (ActividadCulturalDto culturalDto : dtoCulturales) {
            ActividadCultural cultural = mapCultural(culturalDto);
            culturales.add(cultural);
        }

        return culturales;
    }

    public ActividadCultural mapCultural(ActividadCulturalDto dto) {

        Objects.requireNonNull(dto, "dto can't be null");

        ActividadCultural cultural = new ActividadCultural();
        cultural.setActividad(dto.getActividad());
        cultural.setPuntos(dto.getPuntos());
        cultural.setLugar(dto.getLugar());
        cultural.setFecha(new Date(dto.getFecha()));

        FacultadDto facultadDto = dto.getFacultad();
        cultural.setFacultad(facultadDto == null ? "<empty>" : facultadDto.getNombre());

        return cultural;
    }

}
