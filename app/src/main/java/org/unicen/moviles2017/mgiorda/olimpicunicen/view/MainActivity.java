package org.unicen.moviles2017.mgiorda.olimpicunicen.view;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.unicen.moviles2017.mgiorda.olimpicunicen.R;
import org.unicen.moviles2017.mgiorda.olimpicunicen.network.OlimpicApiClient;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.ActividadCultural;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.DiaEventos;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.Partido;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.service.OlimpicService;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.service.ServiceCallback;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EventAdapter<Partido> partidoEventAdapter;
    private EventAdapter<ActividadCultural> culturalEventAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        OlimpicApiClient apiClient = new OlimpicApiClient(getString(R.string.server_url));
        final OlimpicService olimpicService = new OlimpicService(apiClient);

        partidoEventAdapter = new EventAdapter<Partido>() {

            @Override
            public void getEventList(final ServiceCallback<List<DiaEventos<Partido>>> callback) {
                olimpicService.getPartidos(callback);
            }

            @Override
            public String getEventText(Partido evento) {

                String deporte = evento.getDeporte().toUpperCase();
                String facultad1 = evento.getFacultad1();
                String facultad2 = evento.getFacultad2();

                return String.format("%s: %s VS %s", deporte, facultad1, facultad2);
            }

            @Override
            public String getResult(Partido evento) {
                return evento.isJugado() ? String.format("(%s)", evento.getResultado()) : "";
            }

            @Override
            public int getEventIcon(Partido evento) {

                switch (evento.getDeporte()) {

                    case "Basquet":
                        return R.drawable.basketball;
                    case "Voley":
                        return R.drawable.voley;
                    case "Voley Femenino":
                        return R.drawable.voley;
                    case "Ping Pong":
                        return R.drawable.table_tennis;
                    case "Futbol":
                        return R.drawable.football;
                    case "Handball":
                        return R.drawable.handball;

                        //TODO: Estos son errores de tipeo
                    case "Ping Pon":
                        return R.drawable.table_tennis;
                    case "Fultbol":
                        return R.drawable.football;

                    default:
                        return R.drawable.default_sport;
                }
            }
        };

        culturalEventAdapter = new EventAdapter<ActividadCultural>() {

            @Override
            public void getEventList(ServiceCallback<List<DiaEventos<ActividadCultural>>> callback) {
                olimpicService.getCulturales(callback);
            }

            @Override
            public String getEventText(ActividadCultural evento) {
                return String.format("%s: %s", evento.getActividad(), evento.getFacultad());
            }

            @Override
            public String getResult(ActividadCultural evento) {
                return evento.hasPuntos() ? String.format("(%s)", evento.getPuntos()) : "";
            }

            @Override
            public int getEventIcon(ActividadCultural evento) {

                switch (evento.getActividad()) {

                    case "Musica":
                        return R.drawable.music;
                    case "Escultura":
                        return R.drawable.sculpture;
                    case "Pintura":
                        return R.drawable.painting;

                    default:
                        return R.drawable.default_theater;
                }
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast toast = Toast.makeText(MainActivity.this.getApplication(), msg, Toast.LENGTH_LONG);
                View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.toast_custom, null);
                ((TextView) view.findViewById(R.id.icon_text)).setText(msg);
                toast.setView(view);
                toast.show();
            }
        });
    }

    public boolean checkConnectivity() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo localNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return localNetworkInfo != null && localNetworkInfo.isConnected() && localNetworkInfo.isAvailable();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            return (position == 0) ?
                    new EventFragment<Partido>().setEventAdapter(partidoEventAdapter)
                    : new EventFragment<ActividadCultural>().setEventAdapter(culturalEventAdapter);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
