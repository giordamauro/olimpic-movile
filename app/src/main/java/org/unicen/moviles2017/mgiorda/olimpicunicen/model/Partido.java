package org.unicen.moviles2017.mgiorda.olimpicunicen.model;

import android.support.annotation.NonNull;

import java.util.Date;

public class Partido implements Evento {

    private String deporte;

    private String facultad1;
    private String facultad2;

    private Date fecha;
    private String lugar;

    private String resultado;

    public boolean isJugado() {
        return resultado != null;
    }

    public String getDeporte() {
        return deporte;
    }

    public void setDeporte(String deporte) {
        this.deporte = deporte;
    }

    public String getFacultad1() {
        return facultad1;
    }

    public void setFacultad1(String facultad1) {
        this.facultad1 = facultad1;
    }

    public String getFacultad2() {
        return facultad2;
    }

    public void setFacultad2(String facultad2) {
        this.facultad2 = facultad2;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    @Override
    public int compareTo(@NonNull Evento partido) {
        return fecha.compareTo(partido.getFecha());
    }
}
