package org.unicen.moviles2017.mgiorda.olimpicunicen.network;

import org.unicen.moviles2017.mgiorda.olimpicunicen.network.dto.ActividadCulturalDto;
import org.unicen.moviles2017.mgiorda.olimpicunicen.network.dto.PartidoDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface OlimpicAPI {

    @GET("olimpic/getculturales/")
    Call<List<ActividadCulturalDto>> getActividadesCulturales();

    @GET("olimpic/getpartidos/")
    Call<List<PartidoDto>> getPartidos();
}
