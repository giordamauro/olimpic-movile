package org.unicen.moviles2017.mgiorda.olimpicunicen.network.dto;

import java.util.Objects;

public class FacultadDto {

    private int id;
    private String nombre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FacultadDto facultad = (FacultadDto) o;
        return id == facultad.id &&
                Objects.equals(nombre, facultad.nombre);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, nombre);
    }
}
