package org.unicen.moviles2017.mgiorda.olimpicunicen.model;

import java.util.Collections;
import java.util.Date;

public interface Evento extends Comparable<Evento> {

    Date getFecha();

    String getLugar();
}
