package org.unicen.moviles2017.mgiorda.olimpicunicen.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.unicen.moviles2017.mgiorda.olimpicunicen.network.dto.ActividadCulturalDto;
import org.unicen.moviles2017.mgiorda.olimpicunicen.network.dto.PartidoDto;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.service.ServiceCallback;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OlimpicApiClient {

    private final OlimpicAPI olimpicApi;

    public OlimpicApiClient(String baseUrl) {
        this.olimpicApi = getOlimpicApi(baseUrl);
    }

    public void getPartidos(final ServiceCallback<List<PartidoDto>> callback) {

        Objects.requireNonNull(callback, "callback can't be null");

        olimpicApi.getPartidos().enqueue(new Callback<List<PartidoDto>>() {

            @Override
            public void onResponse(Call<List<PartidoDto>> call, Response<List<PartidoDto>> response) {

                Log.i("olimpicapi", "getting Lista Partidos");

                if(response.isSuccessful()) {
                    callback.onSuccess(response.body());
                }
                else {
                    Log.e("olimpicapi", "getPartidos: " + response.message());
                    callback.onError(response.message(), null);
                }
            }

            @Override
            public void onFailure(Call<List<PartidoDto>> call, Throwable t) {

                Log.e("olimpicapi", "getPartidos", t);
                callback.onError(t.getMessage(), t);
            }
        });
    }

    public void getCulturales(final ServiceCallback<List<ActividadCulturalDto>> callback) {

        Objects.requireNonNull(callback, "callback can't be null");

        olimpicApi.getActividadesCulturales().enqueue(new Callback<List<ActividadCulturalDto>>() {

            @Override
            public void onResponse(Call<List<ActividadCulturalDto>> call, Response<List<ActividadCulturalDto>> response) {

                Log.i("olimpicapi", "getting Lista Actividades Culturales");

                if(response.isSuccessful()) {
                    callback.onSuccess(response.body());
                }
                else {
                    Log.e("olimpicapi", "getCulturales: " + response.message());
                    callback.onError(response.message(), null);
                }
            }

            @Override
            public void onFailure(Call<List<ActividadCulturalDto>> call, Throwable t) {

                Log.e("olimpicapi", "getCulturales", t);
                callback.onError(t.getMessage(), t);
            }
        });
    }

    public static OlimpicAPI getOlimpicApi(String baseUrl) {

        Objects.requireNonNull(baseUrl, "baseUrl can't be null");

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(OlimpicAPI.class);
    }
}
