package org.unicen.moviles2017.mgiorda.olimpicunicen.model.service;

public interface ServiceCallback<T> {

    void onSuccess(T response);

    void onError(String message, Throwable exception);
}
