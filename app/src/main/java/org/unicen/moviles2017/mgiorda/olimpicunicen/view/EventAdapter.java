package org.unicen.moviles2017.mgiorda.olimpicunicen.view;

import org.unicen.moviles2017.mgiorda.olimpicunicen.model.DiaEventos;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.Evento;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.service.ServiceCallback;

import java.util.List;

public interface EventAdapter<E extends Evento> {

    void getEventList(ServiceCallback<List<DiaEventos<E>>> callback);

    String getEventText(E evento);

    String getResult(E evento);

    int getEventIcon(E evento);
}
