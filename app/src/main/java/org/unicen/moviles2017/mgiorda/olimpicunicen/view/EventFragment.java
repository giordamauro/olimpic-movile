package org.unicen.moviles2017.mgiorda.olimpicunicen.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import org.unicen.moviles2017.mgiorda.olimpicunicen.R;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.DiaEventos;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.Evento;
import org.unicen.moviles2017.mgiorda.olimpicunicen.model.service.ServiceCallback;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EventFragment<E extends Evento> extends Fragment implements OnRefreshListener {

    private DayEventosListAdapter dayEventosListAdapter;
    private ExpandableListView expandableListView;
    private boolean isLoading = false;

    List<DiaEventos<E>> diaEventos = new ArrayList<>();
    EventAdapter<E> eventAdapter;

    public EventFragment<E> setEventAdapter(EventAdapter<E> eventAdapter) {
        this.eventAdapter = eventAdapter;

        return this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_events, container, false);

        expandableListView = rootView.findViewById(R.id.expandable_list_view);

        SwipeRefreshLayout refresh_layout = rootView.findViewById(R.id.refresh_layout);
        refresh_layout.setOnRefreshListener(this);

        this.dayEventosListAdapter = new DayEventosListAdapter(this.getActivity());
        expandableListView.setAdapter(dayEventosListAdapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {

                if(groupPosition != previousGroup) {
                    expandableListView.collapseGroup(previousGroup);
                }
                previousGroup = groupPosition;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                E partido = diaEventos.get(groupPosition).getEventos().get(childPosition);

                AlertDialog alertDialog = new AlertDialog.Builder(EventFragment.this.getActivity()).create();
                alertDialog.setTitle(eventAdapter.getEventText(partido));
                alertDialog.setMessage(getHoraLugarText(partido));
                alertDialog.show();

                return false;
            }
        });

        this.onRefresh();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void showToast(String message) {
        ((MainActivity) EventFragment.this.getActivity()).showToast(message);
    }

    @Override
    public void onRefresh() {

        if (this.isLoading) {
            showToast(getString(R.string.message_wait_loading));
            return;
        }

        this.isLoading = true;
        final MainActivity mainActivity = (MainActivity) EventFragment.this.getActivity();

        new Thread(new Runnable() {
            public void run() {

                if (mainActivity.checkConnectivity()) {

                    eventAdapter.getEventList(new ServiceCallback<List<DiaEventos<E>>>() {

                        @Override
                        public void onSuccess(List<DiaEventos<E>> response) {

                            diaEventos = response;

                            EventFragment.this.dayEventosListAdapter.notifyDataSetChanged();
                            expandableListView.expandGroup(getExpandableGroupByDate(), true);
                            EventFragment.this.isLoading = false;
                        }

                        @Override
                        public void onError(String message, Throwable exception) {

                            mainActivity.showToast(getString(R.string.message_service_error));
                            EventFragment.this.isLoading = false;
                        }
                    });
                } else {
                    mainActivity.showToast(getString(R.string.message_no_connection));
                }
            }
        }).start();
    }

    private int getExpandableGroupByDate() {

        Date currentDate = getCurrentOrFixedDate();

        int expGroup = 0;

        for (DiaEventos<E> diaEvento : diaEventos) {

            Date fecha = diaEvento.getFecha();
            if(currentDate.compareTo(fecha) == 0) {
                return expGroup;
            }

            expGroup++;
        }

        return diaEventos.size() - 1;
    }

    private Date getCurrentOrFixedDate() {

        SimpleDateFormat onlyDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "AR"));

        String currentDate = getResources().getString(R.string.fixed_date);
        if(currentDate.isEmpty()) {

            currentDate = onlyDateFormat.format(new Date());
        }
        try {
            return onlyDateFormat.parse(currentDate);

        } catch (ParseException e) {

            Log.e("olimpicservice", "Exception parsing date", e);
            return new Date();
        }
    }

    class DayEventosListAdapter extends BaseExpandableListAdapter {

        private LayoutInflater mInflater;

        DayEventosListAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return diaEventos.get(groupPosition).getEventos()
                    .get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return diaEventos.get(groupPosition).getEventos()
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return diaEventos.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return diaEventos.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {

            DiaEventos evento = (DiaEventos) getGroup(groupPosition);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.event_header, null);
            }

            TextView fechaHeader = convertView.findViewById(R.id.fecha);
            fechaHeader.setText(getFechaEvento(evento));

            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final E evento = (E) getChild(groupPosition, childPosition);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.event_item, null);
            }

            ImageView imageIcon = convertView.findViewById(R.id.evento_icon);
            TextView textDeporte = convertView.findViewById(R.id.evento);
            TextView textResultado = convertView.findViewById(R.id.resultado);

            imageIcon.setImageResource(eventAdapter.getEventIcon(evento));
            textDeporte.setText(eventAdapter.getEventText(evento));
            textResultado.setText(eventAdapter.getResult(evento));

            return convertView;
        }
    }

    private String getHoraLugarText(Evento evento) {

        Date date = evento.getFecha();
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm", new Locale("es", "AR"));

        return String.format("Hora: %s - %s", dateFormat.format(date), evento.getLugar());
    }

    private String getFechaEvento(DiaEventos<?> diaEventos) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE (dd/MM)", new Locale("es", "AR"));
        String day = dateFormat.format(diaEventos.getFecha());

        return day.substring(0, 1).toUpperCase() + day.substring(1, day.length());
    }
}