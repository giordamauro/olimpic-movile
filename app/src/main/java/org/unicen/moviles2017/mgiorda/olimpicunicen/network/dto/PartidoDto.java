package org.unicen.moviles2017.mgiorda.olimpicunicen.network.dto;

import java.util.Objects;

public class PartidoDto {

    private int id;

    private String deporte;

    private FacultadDto facultad1;
    private FacultadDto facultad2;

    private String resultado;
    private long fecha;
    private String lugar;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeporte() {
        return deporte;
    }

    public void setDeporte(String deporte) {
        this.deporte = deporte;
    }

    public FacultadDto getFacultad1() {
        return facultad1;
    }

    public void setFacultad1(FacultadDto facultad1) {
        this.facultad1 = facultad1;
    }

    public FacultadDto getFacultad2() {
        return facultad2;
    }

    public void setFacultad2(FacultadDto facultad2) {
        this.facultad2 = facultad2;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public long getFecha() {
        return fecha;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PartidoDto partido = (PartidoDto) o;
        return id == partido.id &&
                fecha == partido.fecha &&
                Objects.equals(deporte, partido.deporte) &&
                Objects.equals(facultad1, partido.facultad1) &&
                Objects.equals(facultad2, partido.facultad2) &&
                Objects.equals(resultado, partido.resultado) &&
                Objects.equals(lugar, partido.lugar);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, deporte, facultad1, facultad2, resultado, fecha, lugar);
    }
}
