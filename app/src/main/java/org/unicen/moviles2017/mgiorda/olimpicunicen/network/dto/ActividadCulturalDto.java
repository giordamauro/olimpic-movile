package org.unicen.moviles2017.mgiorda.olimpicunicen.network.dto;

import java.util.Objects;

public class ActividadCulturalDto {

    private int id;

    private String actividad;
    private FacultadDto facultad;

    private Integer puntos;
    private long fecha;
    private String lugar;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public FacultadDto getFacultad() {
        return facultad;
    }

    public void setFacultad(FacultadDto facultad) {
        this.facultad = facultad;
    }

    public Integer getPuntos() {
        return puntos;
    }

    public void setPuntos(Integer puntos) {
        this.puntos = puntos;
    }

    public long getFecha() {
        return fecha;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActividadCulturalDto that = (ActividadCulturalDto) o;
        return id == that.id &&
                puntos == that.puntos &&
                fecha == that.fecha &&
                Objects.equals(actividad, that.actividad) &&
                Objects.equals(facultad, that.facultad) &&
                Objects.equals(lugar, that.lugar);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, actividad, facultad, puntos, fecha, lugar);
    }
}
