package org.unicen.moviles2017.mgiorda.olimpicunicen.model;

import android.support.annotation.NonNull;

import java.util.Date;

public class ActividadCultural implements Evento {

    private String actividad;
    private String facultad;

    private Date fecha;
    private String lugar;

    private Integer puntos;

    public boolean hasPuntos() {
        return puntos != null;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    public Integer getPuntos() {
        return puntos;
    }

    public void setPuntos(Integer puntos) {
        this.puntos = puntos;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    @Override
    public int compareTo(@NonNull Evento actividadCultural) {
        return fecha.compareTo(actividadCultural.getFecha());
    }
}
