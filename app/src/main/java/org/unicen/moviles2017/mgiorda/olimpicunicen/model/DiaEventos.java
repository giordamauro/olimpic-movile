package org.unicen.moviles2017.mgiorda.olimpicunicen.model;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class DiaEventos<E extends Evento> implements Comparable<DiaEventos<E>> {

    private final Date fecha;
    private final List<E> eventos;

    public DiaEventos(Date fecha, List<E> eventos) {

        Objects.requireNonNull(fecha, "fecha can't be null");
        Objects.requireNonNull(eventos, "eventos can't be null");

        this.fecha = fecha;
        this.eventos = eventos;
    }

    public Date getFecha() {
        return fecha;
    }

    public List<E> getEventos() {
        return eventos;
    }

    @Override
    public int compareTo(@NonNull DiaEventos<E> eDiaEventos) {
        return fecha.compareTo(eDiaEventos.fecha);
    }
}
